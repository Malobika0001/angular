import {Rest} from '../shared/rest'


export const Restuarent:Rest[]=[{
  name:"Skyye Bar",
  id:"1",
  description: "When in bangalore! Go to rooftop bars... Skyye offers a view to the bangalore city and with its round-the-year pleasant weather you can have the best time. They combine great food with good DJ. If you want to celebrate anything hit this place.", 
  location:"link it",
  image:"assets/Skyye.jpg"
},
{
  name:"Toit",
  id:"2",
  description: "It combines its magnificent ambience with brewery. It is usually very hard to get a table on weekends as most of the bangalore crowds hangs out here. The pizzas and desserts are delicious and you can combine it with any custom drink you want",
  location:"link it",
  image:"assets/Toit.jpg"
},
{
  name:"Truffles",
  id:"3",
  description: "Come here for the food- the burgers,coffee,desserts,pizza,sandwichs are out of the world. It is a treat for your taste buds and also hard to get in as it is usually crowded on weekends with 50 mins waiting time! Although I feel it is worth it!",
  location:"link it",
  image:"assets/truffles.jpg"
},
{
  name:"UbCity Rooftop Restaurent",
  id:"4",
  description: "The most luxuriors rooftop bar and restaurent. They have every great cafe up there with a view to die for! It is so picturesque that you will have hard time leaving without pictures as it is located in one of the most elite malls in bangalore the UBcity",
  location:"link it",
  image:"assets/ubcity.jpg"
},
{
  name:"Byg Brewisky",
  id:"5",
  description: "The tables surround a small man-made pond with fishes in it. The music or food will entice you! It is beautifully arranged bar with DJ that can play your favorites!",
  location:"link it",
  image:"assets/byg-brewski.jpg"
},
{
  name:"Prost",
  id:"6",
  description: "If you are on a date please visit this rooftop bar as they can make you feel like you are on the top of the world",
  location:"link it",
  image:"assets/prost.jpg"
},
{
  name:"Justbe Café",
  id:"7",
  description: "The vegan and nutritious delicacies are served here! Do not miss the multi grain pizza. If you are eating clear or trying to loose weight definitely try it out",
  location:"link it",
  image:"assets/justbe.jpg"
},
{
  name:"Zoey's",
  id:"8",
  description: "One of the most instragrammable cafes in Bangalore. It will satisfy your insta as well as your taste buds!", 
  location:"link it",
  image:"assets/zoey.jpg"
},
{
  name:"Chulha Chauki Da Dhaba",
  id:"9",
  description: "By far the best restaurent to get North Indian delicacies! From tandoori to kebabs nothing can disappoint you here.", 
  location:"link it",
  image:"assets/chulha.jpg"
},
{
  name:"Cafe Down The Alley",
  id:"10",
  description: "Another instagramable cafes in Bangalore! Penne Pasta, Mexican veg rice and the best quirky snack options! Must try for after lockdown snack",  
  location:"link it",
  image:"assets/down-the-alley.jpg"
},
];
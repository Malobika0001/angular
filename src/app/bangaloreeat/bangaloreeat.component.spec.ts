import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BangaloreeatComponent } from './bangaloreeat.component';

describe('BangaloreeatComponent', () => {
  let component: BangaloreeatComponent;
  let fixture: ComponentFixture<BangaloreeatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BangaloreeatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BangaloreeatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MumbaistayComponent } from './mumbaistay.component';

describe('MumbaistayComponent', () => {
  let component: MumbaistayComponent;
  let fixture: ComponentFixture<MumbaistayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MumbaistayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MumbaistayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

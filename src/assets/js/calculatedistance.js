function haversine_distance(mk1, mk2,mk3,mk4) {
    var R = 3958.8; // Radius of the Earth in miles
    var rlat1 = mk1 * (Math.PI/180); // Convert degrees to radians
    var rlat2 = mk3 * (Math.PI/180); // Convert degrees to radians
    var difflat = rlat2-rlat1; // Radian difference (latitudes)
    var difflon = (mk2-mk4) * (Math.PI/180); // Radian difference (longitudes)

    var d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat/2)*Math.sin(difflat/2)+Math.cos(rlat1)*Math.cos(rlat2)*Math.sin(difflon/2)*Math.sin(difflon/2)));
    if (d<5){
        return d;
    }
    else{
        return d+3;
    }

   }
//change it to bangalore func
function returnrestact(daysoftrip,budget,latitude,longitude,typeofholday,state){
  dist=[];
   
   variousR=[
    ["Ubcity","2000","p","12.9715895","77.5938698"],//data for 1000 above anf for bangalore
    ["Byg Brewsky","1000","p","12.9129756","77.6809518"],
    ["Church street social","750","p","12.9757069","77.6004488"],
    ["Fenny's","800","p","12.9352239","77.6112638"],
    ["Hard Rock Cafe","1500","p","12.9763894","77.5992796"],
    ["Hoppipola","650","p","12.9752452","77.6012424"],
    ["Opus","1000","p","12.9151537","77.663933"],
    ["Skyye Bar","2000","p","12.9151537","77.663933"],
    ["The Monkey Bar","900","p","12.9704865","77.6430842"],
    ["Tipsy Bull","600","p","12.9704841","77.6102533"],
    ["Toit","1000","p","12.9792966","77.6384555"],
    ["The Black Rabbit","750","p","12.969806","77.6388828"],
    ["Cafe Down the Alley","250","r","12.935414","77.5306993"],
    ["Chulha Chowki Da Dhaba","500","r","12.9685491","77.6158819"],
    ["Guzzlers Inn","350","r","12.973621","77.6049168"],
    ["Justbe","500","r","13.0072005","77.5772126"],
    ["Nandos","900","r","12.9699142","77.635753"],
    ["Prost","900","r","12.9941433","77.6972205"],
    ["Teavilla","500","r","12.912074","77.6452343"],
    ["Truffles","450","r","12.9336352","77.6120463"],
    ["Windmill Craftworks","1500","r","12.965841","77.7161869"],
    ["Zoey","300","r","12.8975268","77.7153493"]

]
variouspR=[
    ["2 Mum's Kitchen","100","p","12.9685931","77.6808701"],
    ["Glen's Bakehouse","300","p","12.9670184","77.7474814"],
    ["Bathinda Tandoor","200","p","12.9072553","77.6071258"],
    ["One Two Cafe","300","r","12.9701558","77.6409389"],
    ["Mokha Cafe Lounge","300","r","12.9363643","77.6116544"]
]
variousA=[
    ["Ubcity","3000","p","12.9715895","77.5938698"],//activity data for all
    ["Bannergatta Biological Park","400","p","12.800285","77.5748587"],
    ["Phoenix","2000","p","12.9957827","77.6941728"],
    ["Nandi Hills","200","p","13.3686666","77.682404"],
    ["Wine Tasting","200","r","13.3783687","77.6778332"],
    ["Microflight Flying","3000","p","13.0769085","77.591034"],
    ["Bangalore Palace","20","r","12.998766","77.5899184"],
    ["Nandi Temple","20","r","12.942898","77.5331777"],
    ["Cubbon Park","0","r","12.9766338","77.5898145"],
    ["ISKCON Temple","0","r","13.0098328","77.5489077"],
    ["Vidhan Sobha","0","r","12.9691226","77.5830984"],
    ["Tipu Sultan Palace","0","r","12.9593513","77.5714528"]
]
    places=[
            ["Whitefield","12.9778272","77.7143842"],//dummy places delete them 
            ["MG Road","12.9756884","77.6035795"],]
    
    for (var i = 0, l1 = variousR.length; i < l1; i++) {
        mk1=variousR[i][3];// loop to calculate distances from one point to another for restaurent
        mk2=variousR[i][4];
        mk3=latitude;
        mk4=longitude;
        var distance = haversine_distance(mk1, mk2,mk3,mk4);
        //console.log("distance is",distance)
        variousR[i][5]=(parseInt(distance)*1.609*300);
       
    }
    //console.log(variousR);
    for (var i = 0, l1 = variouspR.length; i < l1; i++) {
        mk1=variouspR[i][3];// loop to calculate distances from one point to another for restaurent
        mk2=variouspR[i][4];
        mk3=latitude;
        mk4=longitude;
        var distance = haversine_distance(mk1, mk2,mk3,mk4);
        //console.log("distance is",distance)
        variouspR[i][5]=(parseInt(distance)*1.609*300);
       
    }


function sortFunction1(a, b) {
    if (a[6] === b[6]) { 
        return 0;//sort the array according to row 6th money +distance
    }
    else {
        return (a[6] < b[6]) ? -1 : 1;
    }
}

//console.log(variousR);
function sortFunction2(a, b) {
    if (parseInt(a[1]) === parseInt(b[1])) {
        return 0;//sort array according to row 1 that is money
    }
    else {
        return (parseInt(a[1]) < parseInt(b[1])) ? -1 : 1;
    }
}




//budget=5000;
days=daysoftrip;
budgetperday=budget/days;
newarr=[];
newarr=variousR;
newarr.sort(sortFunction2);//sort for money
//cases for different group of money each day
if (parseInt(budgetperday)>2000){
    for (var i = 0, l1 = variousR.length; i < l1; i++) {
        variousR[i][6]=parseInt(variousR[i][5])-parseInt(variousR[i][1]);
    
    }

}
else if(parseInt(budgetperday)<600){
    return ("Nt");

}
else if(parseInt(budgetperday)<1000){
    variousR=variouspR.concat(variousR);
    //console.log( variousR.length);
    variousR[i][6]=parseInt(variousR[i][1])+parseInt(variousR[i][5]);


}
else{
    for (var i = 0, l1 = variousR.length; i < l1; i++) {
        variousR[i][6]=parseInt(variousR[i][1])+parseInt(variousR[i][5]);
       
    
    }

}
//cases end for restaurent
variousR.sort(sortFunction1);

function addactivity(distancefromnewplace1,sum,budgetperday,variousA){
    
   //function for activity
    for (var i = 0, l1 = variousA.length; i < l1; i++) {
        mk1=variousA[i][3];//sort for activity
        mk2=variousA[i][4];
        mk3=latitude;
        mk4=longitude;
        var distance = haversine_distance(mk1, mk2,mk3,mk4);
        variousA[i][5]=(parseInt(distance)*1.609*300)
       
    }
   
    for (var i = 0, l1 = variousA.length; i < l1; i++) {
        variousA[i][6]=parseInt(variousA[i][1])+parseInt(variousA[i][5]);//distance and price for activity
    }
 
    variousA.sort(sortFunction1);
    remaining=budgetperday-sum;
   
    console.log(variousA[0][0]);
    dist.push(variousA[0][0]);
    variousA.splice(0, 1);
    
    return variousA;

}
t=variousR[0][0].length;
//console.log(t);   
jp=[];
for (var i=0,l3=t;i<l3;i++){
    if (parseInt(variousR[i][1])>budgetperday){
        //console.log(parseInt(variousR[i][1]));
       //means budget is too less
    }
    else{
        jp.push(variousR[i]);
        

    }
}
//console.log(jp)
var k=0;
do{
    //loop for restarent
    m=0;
    //console.log(jp[m][0],jp[m+1][0]);
    dist.push(jp[m][0],jp[m+1][0]);
    sum=parseInt(jp[m][1])+parseInt(jp[m+1][1]);
    
 
    variousA=addactivity(jp[m],sum,budgetperday,variousA);
    x_v=2*budgetperday-sum;
    jp.splice(0, m+1);
    jp.splice(0, m+1);
  
    jp.sort(sortFunction2);
    
    if(parseInt(jp[m][1])+parseInt(jp[m+1][1])<x_v+500){
       
       
        console.log(jp[m][0],jp[m+1][0]);
        dist.push(jp[m][0],jp[m+1][0]);
        variousA=addactivity(jp[m],sum,budgetperday,variousA);

       
        jp.splice(0, m+1);
        jp.splice(0, m+1);
    }//add else if for elevated budget
    else if(parseInt(jp[m][1])+parseInt(jp[m+1][1])<x_v+1000){
        console.log(jp[m][0],jp[m+1][0]);
        dist.push(jp[m][0],jp[m+1][0]);
        variousA=addactivity(jp[m],sum,budgetperday,variousA);
        console.log("Have to spend a bit more");
        jp.splice(0, m+1);
        jp.splice(0, m+1);

    }
    else{
        console.log("Please in crease your budget!");
    }
    jp.sort(sortFunction1);
    
    k=k+2;


} while(k<days);

return (dist);
};



//for delhi
function returnrestactdelhi(daysoftrip,budget,latitude,longitude,typeofholday,state){
  dist=[];
    delhiR=[
        ["Diggin","750","r","28.5950381","77.1959753"],
        ["Ichiban","600","r","28.6059318","77.2238928"],
        ["Rose Cafe","700","r","28.5178115","77.1957346"],
        ["Karim","400","r","28.6475357","77.2321136"],
        ["Kake Di Hatti","250","p","28.6475357","77.2321136"],
        ["Sana-Di-Ge","1100","p","28.6026876","77.1858594"],
        ["The Big Chill","1000","p","28.600512","77.2240585"],
        ["Perch Wine & Coffee Bar","1000","p","28.600512","77.2240585"],
        ["Spezia Bistro","450","p","28.6952287","77.2049247"],
        ["Phoebooth Reloaded","650","p","28.6953641","77.2020015"],
        ["Sharmaji Parantha Wala","125","p","28.6727434","77.2031559"],
        ["Barbeque Nation","800","p","28.6286518","77.0743423"],
        ["Oye Khana","50","r","28.7210564","77.1600196"],
        ["Chilli's Grill & Bar","600","r","28.682238","77.0584643"],
        ["Biryani By kilo","250","r","28.5234993","77.0832194"],
        ["Tereso Mio","600","r","28.6437667","77.296361"],
        ["The Salt Cafe Kitchen & Bar","600","r","28.634566","77.2885445"],
        ["Pa Pa Ya","1500","p","28.5284561","77.2168407"],
        ["Bukhara-ITC Maurya","5000","p","28.597062","77.1714477"],
        ["Rajinder Da Dhaba","250","p","28.5656012","77.1971247"]
];
    delhiA=[["Gawking at the Qutab Minar","20","r","28.5244754","77.1833319"],
    ["Find Peace at Lotus Temple","1","r","28.553492","77.2566324"],
    ["Take a walk to India Gate","1","r","28.612912","77.2273157"],
    ["Pay obeyence at the Akshardham temple","1","r","28.6129074","77.1943936"],
    ["Visit the Rashtrapati Bhavan","1","r","28.6143478","77.197236"],
    ["Party away at Connaught Place","r","3000","28.6289017","77.2065107"],
    ["Go boating at the Old Fort","r","200","28.5706135","77.1949614"],
    ["Enjoy a meal at India Habitat Centre","r","200","28.5896808","77.2233662"],
    ["Take a Insta-photo tour of Humayun’s Tomb","p","1","28.5932848","77.2485552"],
    ["Food at Paranthe Wali Gali","p","200","28.6560489","77.2283933"],
    ["Join the chanting at the ISKCON temple","p","200","28.5561132","77.2516366"],
    ["Listen to soul stirring music at Nizamuddin Dargha","p","20","28.5913871","77.2396703"],
    ["Enjoy budget street shopping at Sarojini Nagar Market","p","1000","28.5769069","77.194069"],
    ["Watch a performance at Kamani Auditorium","p","200","28.62323","77.229105"],
    ["Have a ball at Kingdom of Dreams","p","3000","28.4678664","77.0666926"],
    ["Spend an exclusive evening at Kitty Su","p","4000","28.631402","77.2252022"],

];
delhipR=[["Paparizza - Lust For Crust","p","250","28.695573","77.2019874"],
["Ama Cafe","r","200","28.7025873","77.2261855"],
["Saravana Bhavan","r","200","28.6231134","77.1453589"],
["Andhra Bhavan","r","125","28.6168238","77.2236757"]]//add 4 cafe here
   
console.log("fromcal distance",delhiR);
    for (var i = 0, l1 = delhiR.length; i < l1; i++) {
        mk1=delhiR[i][3];// loop to calculate distances from one point to another for restaurent
        mk2=delhiR[i][4];
        mk3=latitude;
        mk4=longitude;
        var distance = haversine_distance(mk1, mk2,mk3,mk4);
        //console.log("distance is",distance)
        delhiR[i][5]=(parseInt(distance)*1.609*300);
       
    }
    //console.log(variousR);
    for (var i = 0, l1 = delhipR.length; i < l1; i++) {
        mk1=delhipR[i][3];// loop to calculate distances from one point to another for restaurent
        mk2=delhipR[i][4];
        mk3=latitude;
        mk4=longitude;
        var distance = haversine_distance(mk1, mk2,mk3,mk4);
        //console.log("distance is",distance)
        delhipR[i][5]=(parseInt(distance)*1.609*300);
       
    }


function sortFunction1(a, b) {
    if (a[6] === b[6]) { 
        return 0;//sort the array according to row 6th money +distance
    }
    else {
        return (a[6] < b[6]) ? -1 : 1;
    }
}

//console.log(variousR);
function sortFunction2(a, b) {
    if (parseInt(a[1]) === parseInt(b[1])) {
        return 0;//sort array according to row 1 that is money
    }
    else {
        return (parseInt(a[1]) < parseInt(b[1])) ? -1 : 1;
    }
}




//budget=5000;
days=daysoftrip;
budgetperday=budget/days;
newarr=[];
newarr=delhiR;
newarr.sort(sortFunction2);//sort for money
//cases for different group of money each day
if (parseInt(budgetperday)>2000){
    for (var i = 0, l1 = delhiR.length; i < l1; i++) {
        delhiR[i][6]=parseInt(delhiR[i][5])-parseInt(delhiR[i][1]);
    
    }

}
else if(parseInt(budgetperday)<600){
    return ("Nt");

}
else if(parseInt(budgetperday)<1000){
    delhiR=delhipR.concat(delhiR);
    //console.log( variousR.length);
    delhiR[i][6]=parseInt(delhiR[i][1])+parseInt(delhiR[i][5]);


}
else{
    for (var i = 0, l1 = delhiR.length; i < l1; i++) {
        delhiR[i][6]=parseInt(delhiR[i][1])+parseInt(delhiR[i][5]);
       
    
    }

}
//cases end for restaurent
delhiR.sort(sortFunction1);

function addactivity(distancefromnewplace1,sum,budgetperday,delhiA){
    
   //function for activity
    for (var i = 0, l1 = delhiA.length; i < l1; i++) {
        mk1=delhiA[i][3];//sort for activity
        mk2=delhiA[i][4];
        mk3=latitude;
        mk4=longitude;
        var distance = haversine_distance(mk1, mk2,mk3,mk4);
        delhiA[i][5]=(parseInt(distance)*1.609*300)
       
    }
   
    for (var i = 0, l1 = delhiA.length; i < l1; i++) {
        delhiA[i][6]=parseInt(delhiA[i][1])+parseInt(delhiA[i][5]);//distance and price for activity
    }
 
    delhiA.sort(sortFunction1);
    remaining=budgetperday-sum;
   
    console.log(delhiA[0][0]);
    dist.push(delhiA[0][0]);
    delhiA.splice(0, 1);
    
    return delhiA;

}
t=delhiR[0][0].length;
//console.log(t);   
jp=[];
for (var i=0,l3=t;i<l3;i++){
    if (parseInt(delhiR[i][1])>budgetperday){
        //console.log(parseInt(variousR[i][1]));
       //means budget is too less
    }
    else{
        jp.push(delhiR[i]);
        

    }
}
//console.log(jp)
var k=0;
do{
    //loop for restarent
    m=0;
    //console.log(jp[m][0],jp[m+1][0]);
    dist.push(jp[m][0],jp[m+1][0]);
    sum=parseInt(jp[m][1])+parseInt(jp[m+1][1]);
    
 
    delhiA=addactivity(jp[m],sum,budgetperday,delhiA);
    x_v=2*budgetperday-sum;
    jp.splice(0, m+1);
    jp.splice(0, m+1);
  
    jp.sort(sortFunction2);
    console.log("jp",jp);
    if(parseInt(jp[m][1])+parseInt(jp[m+1][1])<x_v+500){
       
       
        console.log(jp[m][0],jp[m+1][0]);
        dist.push(jp[m][0],jp[m+1][0]);
        delhiA=addactivity(jp[m],sum,budgetperday,delhiA);

       
        jp.splice(0, m+1);
        jp.splice(0, m+1);
    }//add else if for elevated budget
    else if(parseInt(jp[m][1])+parseInt(jp[m+1][1])<x_v+1000){
        console.log(jp[m][0],jp[m+1][0]);
        dist.push(jp[m][0],jp[m+1][0]);
        delhiA=addactivity(jp[m],sum,budgetperday,delhiA);
        console.log("Have to spend a bit more");
        jp.splice(0, m+1);
        jp.splice(0, m+1);

    }
    else{
        console.log("Please in crease your budget!");
    }
    jp.sort(sortFunction1);
    
    k=k+2;


} while(k<days);

return (dist)
}





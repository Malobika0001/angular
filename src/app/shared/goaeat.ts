import {Rest} from '../shared/rest'


export const Restuarent:Rest[]=[{
  name:"Go With the Flow",
  id:"1",
  description: "It combines the ancient architecture with sea food. It is the perfect place for a date. Do not forget to try Prawn curry", 
  location:"link it",
  image:"assets/gowiththeflow.jpg"
},
{
  name:"The Lazy Goose",
  id:"2",
  description: "Restaurent by the riverside of Nerul. The place feels out of the world. It can make you not wanna go home with its heavenly sunsets and straight out of canvas views. Try the garlic prawns with beverage of your choice!", 
  location:"link it",
  image:"assets/thelazygoose.jpg"
},
{
  name:"Venite Bar & Restaurant",
  id:"3",
  description: "Try this place for authenic portuguese food. Only place in Goa with lot of breakfast options. Hit this place for brunch! Remains closed on Sundays!",
  location:"link it",
  image:"assets/venite.jpg"
},
{
  name:"Gunpowder",
  id:"4",
  description: "Perfect combination of food with quiet ambience. They serve delicious chicken,pork,prawns and fish curry. Do not miss the malabar parotta. The restaurent is usually crowded so plan ahead.",
  location:"link it",
  image:"assets/gunpowder.jpg"
},
{
  name:"Antares",
  id:"5",
  description: "One of the most famous beach side restautents in Goa. It is famous for its food and beachside view. It combines every wish in one- sunsets with live music and a charcoal grill. It is reputed for its Australian cuisines!",
  location:"link it",
  image:"assets/antares.jpg"
},
{
  name:"Bomra's",
  id:"6",
  description: "Recommended by Vogue India it is Burmese Restuarent. From dried shrimp to pomegranate salad it has everything that your taste can ask for!",
  location:"link it",
  image:"assets/bomra-s.jpg"
},
{
  name:"Waterfront Terrace And Bar – The Marriot",
  id:"7",
  description: "It is luxury dining with ocean at one side and exotic food at another! If you want to impress your date or want to celebrate the annivery this is your place", 
  location:"link it",
  image:"assets/waterfront.jpg"
},
{
  name:"Thalassa",
  id:"8",
  description: "Like Antares, this is another exotic beachside joint which serves authentic greek food. Its hilltop position makes it best for that exotic experience you are looking for.",
  location:"link it",
  image:"assets/thalassa.jpg"
},
{
  name:"Ritz Classic",
  id:"9",
  description: "The most distinguished and sort after local restaurent. It serves indian delicacies and Goan food for everyone who is looking for budget friendly options in town. Try its fish fry along with chicken! There are also lot of vegetarian options available",
  location:"link it",
  image:"assets/ritz-classic.jpg"
},
{
  name:"Sakana",
  id:"10",
  description: "Do not complete your trip without going to this Japenese restaurent for sushi, rolls and Teriyaki Chicken", 
  location:"link it",
  image:"assets/sakana.jpg"
},
];
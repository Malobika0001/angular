import { Stay } from './stay';

export const stay:Stay[]=[{
    name:"Central Delhi",
    id:"1",
    description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
    hotel1:"https://www.booking.com/hotel/in/radisson-marina-connaught-place.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=27054003_126185350_0_1_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=-2106102;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=27054003_126185350_0_1_0;hp_group_set=0;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=27054003_126185350_0_1_0__479920;srepoch=1596896261;srpvid=2c456482720f01d6;type=total;ucfs=1&#hotelTmpl",
    img1:"../assets/centraldelhi1.jpg",
    price1:"5000",
  
    hotel2:"https://www.booking.com/hotel/in/srivinayak.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=71976003_271943270_2_2_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=-2106102;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=25;highlighted_blocks=71976003_271943270_2_2_0;hpos=25;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=71976003_271943270_2_2_0__118080;srepoch=1596896455;srpvid=c84b64e36400019b;type=total;ucfs=1&#hotelTmpl",
    price2:"1200",
    img2:"../assets/centraldelhi2.jpg",
    hotel3:"https://www.booking.com/hotel/in/the-park-new-delhi.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=25607402_178188139_2_41_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=-2106102;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=4;highlighted_blocks=25607402_178188139_2_41_0;hpos=4;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=25607402_178188139_2_41_0__499900;srepoch=1596896455;srpvid=c84b64e36400019b;type=total;ucfs=1&#hotelTmpl",
    price3:"5000",
    img3:"../assets/centraldelhi3.jpg",
    image:"assets/central-delhi.jpg"
  },
  {
    name:"South Delhi",
    id:"1",
    description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
    hotel1:"https://www.booking.com/hotel/in/hilton-garden-inn-new-delhi-saket.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=26668201_246058337_2_42_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=-2106102;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=8;highlighted_blocks=26668201_246058337_2_42_0;hpos=8;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=26668201_246058337_2_42_0__419930;srepoch=1596896455;srpvid=c84b64e36400019b;type=total;ucfs=1&#hotelTmpl",
    price1:"4200",
    img1:"../assets/southdelhi1.jpg",
    hotel2:"https://www.booking.com/hotel/in/the-grand-new-delhi.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=27052901_261839391_2_41_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=-2106102;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=13;highlighted_blocks=27052901_261839391_2_41_0;hpos=13;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=27052901_261839391_2_41_0__699950;srepoch=1596896455;srpvid=c84b64e36400019b;type=total;ucfs=1&#hotelTmpl",
    price2:"7000",
    img2:"../assets/southdelhi2.jpg",
    hotel3:"https://www.booking.com/hotel/in/minimalist-poshtel-amp-suites.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=614410606_266033190_4_1_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=-2106102;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=9;highlighted_blocks=614410606_266033190_4_1_0;hpos=9;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=614410606_266033190_4_1_0__243000;srepoch=1596907470;srpvid=257b7a6694da0039;type=total;ucfs=1&#hotelTmpl",
    price3:"2500",
    img3:"../assets/southdelhi3.jpg",
    image:"assets/South-delhi.jpg"
  },
  {
    name:"North Delhi",
    id:"1",
    description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
    hotel1:"https://www.booking.com/hotel/in/haveli-dharampura.en-gb.html?aid=306395;label=new-delhi%2Fnorth-delhi-zVlPde_8p5_z2rTQ4omSCAS151853229762%3Apl%3Ata%3Ap130%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-4234455547%3Alp9061658%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=171143104_272219421_0_41_0;checkin=2020-09-08;checkout=2020-09-09;dest_id=-2106102;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=171143104_272219421_0_41_0;hp_group_set=0;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=171143104_272219421_0_41_0__850000;srepoch=1597305862;srpvid=a79538c220b301fc;type=total;ucfs=1&#hotelTmpl", 
    price1:"8500",
    img1:"../assets/northdelhi1.jpg",
    hotel2:"https://www.booking.com/hotel/in/street-wall.en-gb.html?aid=306395;label=new-delhi%2Fnorth-delhi-alm254hAu9PANB6jnK6NpAS151853229762%3Apl%3Ata%3Ap130%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-11033909227%3Alp9061658%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=650822101_266088331_2_2_0;checkin=2020-09-08;checkout=2020-09-09;dest_id=2185;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=10;highlighted_blocks=650822101_266088331_2_2_0;hpos=10;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=650822101_266088331_2_2_0__153930;srepoch=1597315012;srpvid=f6964aa16e1e0132;type=total;ucfs=1&#hotelTmpl",
    price2:"1500",
    img2:"../assets/northdelhi2.jpg",
    hotel3:"https://www.booking.com/hotel/in/aiwan-e-shahi.en-gb.html?aid=306395;label=new-delhi%2Fnorth-delhi-alm254hAu9PANB6jnK6NpAS151853229762%3Apl%3Ata%3Ap130%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-11033909227%3Alp9061658%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=197061403_262801098_0_41_0;checkin=2020-09-08;checkout=2020-09-09;dest_id=2185;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=3;highlighted_blocks=197061403_262801098_0_41_0;hpos=3;nflt=pri%3D1%3B;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=197061403_262801098_0_41_0__267168;srepoch=1597315453;srpvid=b3c94b7e809c0142;type=total;ucfs=1&#hotelTmpl",
    price3:"2600",
    img3:"../assets/northdelhi3.jpg",
    image:"../assets/north-delhi.jpg"
  },
  {
    name:"West Delhi",
    id:"1",
    description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
    hotel1:"https://www.booking.com/hotel/in/holiday-inn-new-delhi-international-airport.html?aid=318615&label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm&sid=a9f46897da2eafd58958e31c6200c0ee&all_sr_blocks=76970616_270151520_2_41_0&checkin=2020-09-10&checkout=2020-09-11&dest_id=-2106102&dest_type=city&group_adults=2&group_children=0&hapos=5&highlighted_blocks=76970616_270151520_2_41_0&hpos=5&no_rooms=1&sr_order=popularity&sr_pri_blocks=76970616_270151520_2_41_0__413250&srepoch=1596896455&srpvid=c84b64e36400019b&ucfs=1&from=searchresults;highlight_room=;has_campaign_deals_traveloffer20_customer_label=1#hotelTmpl",
    price1:"4200",
    img1:"../assets/westdelhi1.jpg",
    hotel2:"https://www.booking.com/hotel/in/kelvish-plaza.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=442433708_228136698_2_41_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=2474;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=4;highlighted_blocks=442433708_228136698_2_41_0;hpos=4;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=442433708_228136698_2_41_0__119000;srepoch=1596908014;srpvid=2bd87b767e9c00e4;type=total;ucfs=1&#hotelTmpl",
    price2:"1200",
    img2:"../assets/westdelhi2.jpg",
    hotel3:"https://www.booking.com/hotel/in/smart-stay-mahipalpur.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=383655004_148218268_2_42_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=2474;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=9;highlighted_blocks=383655004_148218268_2_42_0;hpos=9;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=383655004_148218268_2_42_0__157500;srepoch=1596908014;srpvid=2bd87b767e9c00e4;type=total;ucfs=1&#hotelTmpl",
    price3:"1500",
    img3:"../assets/westdelhi3.jpg",
    image:"assets/West-delhi.jpg"
  },
  {
    name:"East Delhi",
    id:"1",
    description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
    hotel1:"https://www.booking.com/hotel/in/hilton-new-delhi-mayur-vihar.en-gb.html?aid=306395;label=new-delhi%2Feastdelhi-25XCJYyRSqmjDPw8zTgucAS392972670896%3Apl%3Ata%3Ap120%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-2701564190%3Alp9061658%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;dest_id=2473;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=3;hpos=3;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;srepoch=1597305479;srpvid=f6963803fc160090;type=total;ucfs=1&#hotelTmpl",
    price1:"7000",
    img1:"../assets/eastdelhi1.jpg",
    hotel2:"https://www.booking.com/hotel/in/ginger-hotels-new-delhi-vivek-vihar.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=27988101_205220188_0_2_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=2473;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=10;highlighted_blocks=27988101_205220188_0_2_0;hpos=10;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=27988101_205220188_0_2_0__251928;srepoch=1596908236;srpvid=88807be532e6002e;type=total;ucfs=1&#hotelTmpl",
    price2:"2500",
    img2:"../assets/eastdelhi2.jpg",
    hotel3:"https://www.booking.com/hotel/in/luxury-inn-delhi.html?aid=318615;label=Low_CPA_English_EN_IN_21456997465-nQ0UFjwj5kZh51vO2tXu%2AQS217270754861%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi2653294844%3Atidsa-303068456341%3Alp9061692%3Ali%3Adec%3Adm;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=164059001_194831829_6_42_0;checkin=2020-09-10;checkout=2020-09-11;dest_id=2473;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=22;highlighted_blocks=164059001_194831829_6_42_0;hpos=22;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=164059001_194831829_6_42_0__375000;srepoch=1596908236;srpvid=88807be532e6002e;type=total;ucfs=1&#hotelTmpl",
    price3:"3750",
    img3:"../assets/eastdelhi3.jpg",
    image:"assets/East-delhi.jpg"
  },
  {
    name:"Delhi-NCR",
    id:"1",
    description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
    hotel1:"https://www.booking.com/hotel/in/vivanta-by-taj-gurgaon.en-gb.html?aid=306395&label=gurgaon-svpAtZx2VPPITuhjmoysWQS392865643390%3Apl%3Ata%3Ap1360%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-2701679570%3Alp9061658%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c&sid=a9f46897da2eafd58958e31c6200c0ee&lp_sr_snippet=1",
    price1:"5500",
    img1:"../assets/ncr1.jpg",
    hotel2:"https://www.booking.com/hotel/in/treebo-tryst-mavens-orange.en-gb.html?aid=306395&label=gurgaon-svpAtZx2VPPITuhjmoysWQS392865643390%3Apl%3Ata%3Ap1360%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-2701679570%3Alp9061658%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c&sid=a9f46897da2eafd58958e31c6200c0ee&lp_sr_snippet=1",
    price2:"1140",
    img2:"../assets/ncr2.jpg",
    hotel3:"https://www.booking.com/hotel/in/the-westin-gurgaon.en-gb.html?aid=306395&label=gurgaon-svpAtZx2VPPITuhjmoysWQS392865643390%3Apl%3Ata%3Ap1360%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-2701679570%3Alp9061658%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c&sid=a9f46897da2eafd58958e31c6200c0ee&lp_sr_snippet=1",
    price3:"5400",
    img3:"../assets/ncr3.jpg",
    image:"assets/delhi-ncr.jpg"
  }
]  

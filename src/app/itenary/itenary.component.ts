import { Component, OnInit,ViewChild,Input, Inject } from '@angular/core';
import{itenary} from '../shared/itenary';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import{returnformat} from "../shared/returnformat";
import{FormdisplayService } from "../formdisplay.service";
import {returnformatitenary} from "../shared/returnformatitenary";
import { Router } from '@angular/router';
import { Params, ActivatedRoute, ɵangular_packages_router_router_h } from '@angular/router';
import { switchMap } from 'rxjs/operators';
declare function returnrestact(daysoftrip,budget,latitude,longitude,typeofholiday,state): any;
declare function returnrestactdelhi(daysoftrip,budget,latitude,longitude,typeofholiday,state): any;

interface City{
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-itenary',
  templateUrl: './itenary.component.html',
  styleUrls: ['./itenary.component.scss']
})

export class ItenaryComponent implements OnInit {
 
  feedbackForm: FormGroup;
  feedback: itenary;
  itemstodisplay: string[];
  lenghty:number[];
  possiblel=[];//array to be retured
  latitude="22.7";
  longitude="34.8";
  multitude=true;
  lessbudget=0;
  example:returnformat;
  examplecopy:returnformat;
  dishIds: string[];
  prev: string;
  next: string;
  statevar:string;

  public data:any=[];

  example1:returnformat[] = returnformatitenary;

  
  states: City[] = [
    {value: 'Bangalore', viewValue: 'Bangalore'},
    {value: 'Delhi', viewValue: 'Delhi'},
    {value: 'Mumbai', viewValue: 'Mumbai'},
    {value: 'Goa', viewValue: 'Goa'}
  ];
  
  
  @ViewChild('fform') feedbackFormDirective;
  
  errMess: any;
  counter:Number;
  

  constructor(private fb: FormBuilder,private data1: FormdisplayService,  private route: ActivatedRoute,@Inject('BaseURL') private baseURL) {
    this.createForm();
  }

  ngOnInit() {
  
  }
  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  
  
  createForm() {
    this.feedbackForm = this.fb.group({
      daysoftrip:['',Validators.required],
      budget:['',Validators.required],
      location:['',Validators.required],
      typeofholiday:['',Validators.required],
      state:['',Validators.required]
    });
  }
checkcitylimits(latitude,longitude,state){
  if (state=="Bangalore"){
    if ((parseFloat(latitude)>12.70 && parseFloat(latitude)<13.20 )&& (parseFloat(longitude)>77.40 && parseFloat(longitude)<77.70)){
      console.log("bl");
      return true;

    }
    else{
      console.log("nbl");
      return false;
    }
  }
  else if (state=="Delhi"){
    if ((parseFloat(latitude)>28.20 && parseFloat(latitude)<28.80) && (parseFloat(longitude)>77.20 && parseFloat(longitude)<77.6)){
      console.log("dl");
      return true;

    }
    else{
      console.log("ndl");
      return false;
    }
  }
  else{
    console.log("n-anywhere");
    return false;
  }

}

formErrors = {
  'daysoftrip': '',
  'budget': '',
  'location': '',
  'typeofholiday': ''
};

validationMessages = {
  'daysoftrip': {
    'required':      'Duration of trip is required.',
  
  },
  'budget': {
    'required':      'Budget is required.',
    'pattern':       'Tel. number must contain only numbers.'
   
  },
  'location': {
    'required':      'Location is required.',
  
  },
 
};


  
  checklocation(){
  if (this.feedbackForm.value.location.indexOf("https") !== -1){
        
    var array1 = this.feedbackForm.value.location.split("@");
    var newlocation=array1[1];
    var newl=newlocation.split(",");
    this.latitude=newl[0];
    var newl1= newl[1].split(',');
    this.longitude=newl1[0];
    console.log(this.latitude,this.longitude);
    return true;
}
else if(this.feedbackForm.value.location.indexOf(",") !== -1){
    var array1=this.feedbackForm.value.location.split(",");
    this.latitude=array1[0];
    this.longitude=array1[1];
    
    console.log(this.latitude,this.longitude);
    return true;

}
else{
    console.log("Please enter proper location")}
    return false;
}

  onSubmit() {
    this.lessbudget=0;
    console.log(this.lessbudget);

    if(this.feedbackForm.status=="VALID" && this.checklocation() && this.checkcitylimits(this.latitude,this.longitude,this.feedbackForm.value.state)){
      this.multitude=false;
      this.statevar=this.feedbackForm.value.state;
      if (this.feedbackForm.value.state=="Bangalore"){
        if(returnrestact(this.feedbackForm.value.daysoftrip,this.feedbackForm.value.budget,this.latitude,this.longitude,this.feedbackForm.value.typeofholiday,this.feedbackForm.value.state)[0]=="N"){
          this.lessbudget=1;//less budget warning
          console.log(this.lessbudget,"less budget")
        }
        
        
        this.itemstodisplay=returnrestact(this.feedbackForm.value.daysoftrip,this.feedbackForm.value.budget,this.latitude,this.longitude,this.feedbackForm.value.typeofholiday,this.feedbackForm.value.state);
        var counter=0;
        for (var num1=0;num1<3*(this.feedbackForm.value.daysoftrip);num1=num1+3){
          this.possiblel.push(num1);
          let customObj = new returnformat();
         
          customObj.id=counter.toString();
          customObj.restaurent1=this.itemstodisplay[num1];
  
          customObj.restaurent2=this.itemstodisplay[num1+1];
  
          customObj.activity=this.itemstodisplay[num1+2];
  
          this.example1.push(customObj);
          this.data1.putreturnformat(customObj)
        .subscribe(customObj=> {
          customObj = customObj; customObj = customObj;
        },
        errmess => { this.example1= null; this.example1 = null; this.errMess = <any>errmess; });
            console.log(this.example1,"here we go");
           counter=counter+1;
     }            localStorage.setItem('testObject', JSON.stringify(this.example1));
   


      }
      else if(this.feedbackForm.value.state=="Delhi"){
        var latitude1=this.latitude

        this.statevar=this.feedbackForm.value.state;
        if(returnrestactdelhi(this.feedbackForm.value.daysoftrip,this.feedbackForm.value.budget,latitude1,this.longitude,this.feedbackForm.value.typeofholiday,this.feedbackForm.value.state)[0]=="N"){
          this.lessbudget=1;//less budget warning
          console.log(this.lessbudget,"less budget")
        }
        
        this.itemstodisplay=returnrestactdelhi(this.feedbackForm.value.daysoftrip,this.feedbackForm.value.budget,latitude1,this.longitude,this.feedbackForm.value.typeofholiday,this.feedbackForm.value.state);
        var counter=0;
        for (var num1=0;num1<3*(this.feedbackForm.value.daysoftrip);num1=num1+3){
          this.possiblel.push(num1);
          let customObj = new returnformat();
         
          customObj.id=counter.toString();
          customObj.restaurent1=this.itemstodisplay[num1];
  
          customObj.restaurent2=this.itemstodisplay[num1+1];
  
          customObj.activity=this.itemstodisplay[num1+2];
  
          this.example1.push(customObj);
          this.data1.putreturnformat(customObj)
        .subscribe(customObj=> {
          customObj = customObj; customObj = customObj;
        },
        errmess => { this.example1= null; this.example1 = null; this.errMess = <any>errmess; });
            console.log(this.example1,"here we go");
           counter=counter+1;
     }            localStorage.setItem('testObject', JSON.stringify(this.example1));
   

    



      }
      else{
        console.log("here");//add goa and mumbai
      }
     
   
  
   
      
    }
    else{
      this.lessbudget=2;
      console.log(this.lessbudget);
    }
   
   
   
    this.feedback = this.feedbackForm.value;


   
    this.feedbackForm.reset({
      daysoftrip:'',
      budget:'',
      location:'',
      typeofholiday:'',
      state:''
      

    }
    
    
    );
    
  }

 



  
  

}

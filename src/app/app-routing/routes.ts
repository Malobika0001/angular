import { Routes } from '@angular/router';

import { PlacesComponent } from '../places/places.component';
import { DelhieatComponent } from '../delhieat/delhieat.component';
import{DelhistayComponent} from '../delhistay/delhistay.component';
import {MumbaieatComponent} from '../mumbaieat/mumbaieat.component';
import {MumbaistayComponent} from '../mumbaistay/mumbaistay.component';
import {BangaloreeatComponent} from '../bangaloreeat/bangaloreeat.component';
import {BangalorestayComponent} from '../bangalorestay/bangalorestay.component';
import {GoaeatComponent} from '../goaeat/goaeat.component';
import {GoastayComponent} from '../goastay/goastay.component';
import {ItenaryComponent} from '../itenary/itenary.component';
import {FormresultsComponent} from '../formresults/formresults.component'

  
export const routes: Routes = [
  { path: 'places',  component: PlacesComponent },
  { path: 'delhieat', component: DelhieatComponent },
  {path:"mumbaieat", component:MumbaieatComponent},
  {path:'mumbaistay', component:MumbaistayComponent},
  { path:'delhistay', component:DelhistayComponent},
  { path:'bangaloreeat', component:BangaloreeatComponent},
  { path:'bangalorestay', component:BangalorestayComponent},
  { path:'itenary', component:ItenaryComponent},
  {path:'goaeat',component:GoaeatComponent},
  {path:'goastay', component:GoastayComponent},
  { path:'formresult',component:FormresultsComponent},
  
  { path: '', redirectTo: '/places', pathMatch: 'full' }
]
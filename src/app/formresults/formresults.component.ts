import { Component, OnInit, Inject } from '@angular/core';
import {returnformatitenary} from"../shared/returnformatitenary";
import { returnformat } from '../shared/returnformat';
import{FormdisplayService } from "../formdisplay.service";
@Component({
  selector: 'app-formresults',
  templateUrl: './formresults.component.html',
  styleUrls: ['./formresults.component.scss']
})
export class FormresultsComponent implements OnInit {
 
  returnformat:returnformat[]=returnformatitenary;
  look:returnformat;
  errMess: string;
  constructor(private FormdisplayService : FormdisplayService , @Inject('BaseURL') private baseURL,) { }
  ngOnInit() {(
    this.FormdisplayService.getreturnformat().subscribe(returnformat=>this.returnformat=returnformat,errmess => this.errMess = <any>errmess));
    var retrievedObject = localStorage.getItem('testObject');
    this.look=JSON.parse(retrievedObject);
    console.log('retrievedObject: ', JSON.parse(retrievedObject));
  
  }
 
}




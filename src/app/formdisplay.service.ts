import { Injectable, Inject } from '@angular/core';
import {returnformat} from'./shared/returnformat';
import {returnformatitenary} from './shared/returnformatitenary';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

import { map,catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from './shared/baseurl';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FormdisplayService {


  getreturnformat(): Observable<returnformat[]> {
    return this.http.get<returnformat[]>(baseURL +'returnformatitenary')
    .pipe(catchError(this.processHTTPMsgService.handleError));;
  }
  constructor(private http: HttpClient, private processHTTPMsgService: ProcessHTTPMsgService,@Inject('BaseURL') private baseURL) { }

  putreturnformat(returnformat1: returnformat): Observable<returnformat> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.put<returnformat>(baseURL + '/formresult',returnformat1, httpOptions)
      .pipe(catchError(this.processHTTPMsgService.handleError));}

    

}

import {Stay} from "../shared/stay"


export const stay:Stay[]=[{
  name:"Calangute",
  id:"1",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/magpie-2bhk-apartment-with-pool.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee",
  img1:"../assets/calangute1.jpg",
  price1:"1500",

  hotel2:"https://www.booking.com/hotel/in/sarala-crown.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee", 
  price2:"2500",
  img2:"../assets/calangute2.jpg",
  hotel3:"https://www.booking.com/hotel/in/le-meridien-goa-calangute.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee",  
  price3:"6000",
  img3:"../assets/calangute3.jpg",
  image:"assets/calangute.jpg"
},
{
  name:"Candolim",
  id:"2",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/silver-sands-serenity.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee", 
  price1:"3600",
  img1:"../assets/candolim1.jpg",
  hotel2:"https://www.booking.com/hotel/in/10-candolim-goa.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=615160801_240480306_4_0_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=-2092526;dest_type=city;dist=0;from_beach_non_key_ufi_sr=1;group_adults=2;group_children=0;hapos=7;highlighted_blocks=615160801_240480306_4_0_0;hpos=7;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=615160801_240480306_4_0_0__99900;srepoch=1597845662;srpvid=fed6628eb7f3013b;type=total;ucfs=1&#hotelTmpl",
  price2:"999",
  img2:"../assets/candolim2.jpg",
  hotel3:"https://www.booking.com/hotel/in/hyatt-centric-candolim-goa.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=156035303_269975823_2_41_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=-2092526;dest_type=city;dist=0;from_beach_non_key_ufi_sr=1;group_adults=2;group_children=0;hapos=6;highlighted_blocks=156035303_269975823_2_41_0;hpos=6;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=156035303_269975823_2_41_0__684250;srepoch=1597845662;srpvid=fed6628eb7f3013b;type=total;ucfs=1&#hotelTmpl",
  price3:"6000",
  img3:"../assets/candolim3.jpg",
  image:"assets/candolim.jpg"
},
{
  name:"Baga",
  id:"3",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/double-tree-by-hilton-goa.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=40378901_266007693_2_42_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=4127;dest_type=region;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=40378901_266007693_2_42_0;hpos=1;nflt=uf%3D900048319%3B;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=40378901_266007693_2_42_0__332500;srepoch=1597845944;srpvid=5a6c631c75e90037;type=total;ucfs=1&#hotelTmpl",
  price1:"3300",
  img1:"../assets/baga1.jpg",
  hotel2:"https://www.booking.com/hotel/in/fiesta-resort.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=203942004_208435431_0_9_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=4127;dest_type=region;dist=0;group_adults=2;group_children=0;hapos=2;highlighted_blocks=203942004_208435431_0_9_0;hpos=2;nflt=uf%3D900048319%3B;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=203942004_208435431_0_9_0__350000;srepoch=1597845944;srpvid=5a6c631c75e90037;type=total;ucfs=1&#hotelTmpl", 
  price2:"3500",
  img2:"../assets/baga2.jpg",
  hotel3:"https://www.booking.com/hotel/in/victoria-baga.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=340533701_203952497_2_0_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=4127;dest_type=region;dist=0;group_adults=2;group_children=0;hapos=8;highlighted_blocks=340533701_203952497_2_0_0;hpos=8;nflt=uf%3D900048319%3B;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=340533701_203952497_2_0_0__159200;srepoch=1597845944;srpvid=5a6c631c75e90037;type=total;ucfs=1&#hotelTmpl",
  price3:"1500",
  img3:"../assets/baga3.jpg",
  image:"assets/baga.jpg"
},
{
  name:"South Goa",
  id:"4",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/dempo-river-sal.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=296516108_265666223_2_1_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=4127;dest_type=region;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=296516108_265666223_2_1_0;hpos=1;nflt=uf%3D-2092554%3B;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=296516108_265666223_2_1_0__238000;srepoch=1597846733;srpvid=8b1e64a6d9b3024d;type=total;ucfs=1&#hotelTmpl",
  price1:"2500",
  img1:"../assets/southgoa1.jpg",
  hotel2:"https://www.booking.com/hotel/in/holiday-inn-resort.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=39710414_270078302_2_42_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=4127;dest_type=region;dist=0;group_adults=2;group_children=0;hapos=7;highlighted_blocks=39710414_270078302_2_42_0;hpos=7;nflt=uf%3D-2092554%3B;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=39710414_270078302_2_42_0__584521;srepoch=1597846733;srpvid=8b1e64a6d9b3024d;type=total;ucfs=1&#hotelTmpl", 
  price2:"6000",
  img2:"../assets/southgoa2.jpg",
  hotel3:"https://www.booking.com/hotel/in/haathi-mahal.en-gb.html?aid=324798;label=goa-MTl4kK1vHKZcSMOHhpttewS388387535727%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-46144001%3Alp9075215%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YcWHiCDB7igB8XpJKYgu4TM;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=39522402_237262508_0_33_0;checkin=2020-10-23;checkout=2020-10-24;dest_id=-2092554;dest_type=city;dist=0;from_beach_non_key_ufi_sr=1;group_adults=2;group_children=0;hapos=5;highlighted_blocks=39522402_237262508_0_33_0;hpos=5;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=39522402_237262508_0_33_0__637500;srepoch=1597846935;srpvid=d258650b03a9027f;type=total;ucfs=1&#hotelTmpl", 
  price3:"6000",
  img3:"../assets/southgoa3.jpg",
  image:"assets/southgoa.jpg"
},


];
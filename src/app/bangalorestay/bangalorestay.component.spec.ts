import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BangalorestayComponent } from './bangalorestay.component';

describe('BangalorestayComponent', () => {
  let component: BangalorestayComponent;
  let fixture: ComponentFixture<BangalorestayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BangalorestayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BangalorestayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

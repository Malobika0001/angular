import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatExpansionModule} from '@angular/material/expansion';
import {DelhieatComponent} from './delhieat/delhieat.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldControl } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms'; 
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ReactiveFormsModule } from '@angular/forms';
import {MatOptionModule,MatIconModule} from '@angular/material'
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { baseURL } from './shared/baseurl';
import { FormdisplayService} from './formdisplay.service';
import '../polyfills';

import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {HttpClientModule} from '@angular/common/http';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,


  MatDialogModule,

 
  MatListModule,

  MatNativeDateModule,
  
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
 

  MatSliderModule,

 
  MatSortModule,

  MatTableModule,
 

  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';


@NgModule({
  exports: [
    CdkTableModule,
    CdkTreeModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
   

    
    MatDialogModule,
   
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
 
   
   
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,

    MatSliderModule,
    MatSlideToggleModule,
 
    MatSortModule,
    MatTableModule,
   
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
 
  ],
  declarations: [],
  
})
export class DemoMaterialModule {}


/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */


import { CookieService } from 'ngx-cookie-service';
import 'hammerjs';
import { PlacesComponent } from './places/places.component';
import { DelhistayComponent } from './delhistay/delhistay.component';
import { MumbaieatComponent } from './mumbaieat/mumbaieat.component';
import { MumbaistayComponent } from './mumbaistay/mumbaistay.component';
import { BangaloreeatComponent } from './bangaloreeat/bangaloreeat.component';
import { BangalorestayComponent } from './bangalorestay/bangalorestay.component';
import { GoaeatComponent } from './goaeat/goaeat.component';
import { GoastayComponent } from './goastay/goastay.component';
import { ItenaryComponent } from './itenary/itenary.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormresultsComponent } from './formresults/formresults.component';
import { CommonModule } from '@angular/common';
import { ProcessHTTPMsgService } from './process-httpmsg.service';





@NgModule({
  declarations: [
    AppComponent,
    PlacesComponent,
    DelhieatComponent,
    DelhistayComponent,
    MumbaieatComponent,
    MumbaistayComponent,
    BangaloreeatComponent,
    BangalorestayComponent,
    GoaeatComponent,
    GoastayComponent,
    ItenaryComponent,
    HeaderComponent,
    FooterComponent,
    FormresultsComponent,
   
   

  


  
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    AppRoutingModule,
    MatFormFieldModule, 
    MatInputModule,
    MatCheckboxModule,
    FormsModule ,
    MatSelectModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    MatButtonToggleModule,
    MatOptionModule,
   
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DemoMaterialModule,
    MatNativeDateModule,
    HttpClientModule 

  
   
  ],
  providers: [
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {floatLabel: 'auto'}},  {provide: 'BaseURL', useValue: baseURL},{provide:'ProcessHTTPMsgService',useValue:ProcessHTTPMsgService },{provide:'Formdisplay',useValue:FormdisplayService},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

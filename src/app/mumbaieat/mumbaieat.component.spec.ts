import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MumbaieatComponent } from './mumbaieat.component';

describe('MumbaieatComponent', () => {
  let component: MumbaieatComponent;
  let fixture: ComponentFixture<MumbaieatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MumbaieatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MumbaieatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

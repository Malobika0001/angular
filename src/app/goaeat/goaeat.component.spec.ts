import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoaeatComponent } from './goaeat.component';

describe('GoaeatComponent', () => {
  let component: GoaeatComponent;
  let fixture: ComponentFixture<GoaeatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoaeatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoaeatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

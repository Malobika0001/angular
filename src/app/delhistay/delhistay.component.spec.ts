import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelhistayComponent } from './delhistay.component';

describe('DelhistayComponent', () => {
  let component: DelhistayComponent;
  let fixture: ComponentFixture<DelhistayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelhistayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelhistayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

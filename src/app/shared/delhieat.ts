import { Rest } from './rest';

export const Restuarent:Rest[]=[{
    name:"Diggin",
    id:"1",
    description: "If you like a great ambience and would love to ditch the mundane cafes for a change visit Diggin! It is picturquese and perfect for a brunch. It is located at the center of city so you do not have to worry about the traffic!",
    location:"link it",
    image:"assets/Diggin.jpg"
  },
  {
    name:"Lord of the drinks meadow",
    id:"2",
    description: "Lord of the drinks is famous for being a do-to bar for any special occassion. So if you really want to impress someone, this is your place. It is the perfect spot for you as you can have that drink with view of the deer park in hauz khas",
    location:"link it",
    image:"assets/Lodmeadow.jpg"
  },
  {
    name:"Fio country kitchen and bar",
    id:"3",
    description: "Come here for food stay for everything else! ",
    location:"link it",
    image:"assets/Fio.jpg"
  },
  {
    name:"Ichiban",
    id:"4",
    description: "Located in pandara road ",
    location:"link it",
    image:"assets/Iciban.jpg"
  },
  {
    name:"Rose Cafe",
    id:"5",
    description: "The cafe can make you feel you are in victorian architecture. The flowers help the place come alive. The perfect place for the perfect date-night",
    location:"link it",
    image:"assets/RoseCafe.jpg"
  },
  {
    name:"Hillside cafe",
    id:"6",
    description: " the finest place in delhi",
    location:"link it",
    image:"assets/Hillside.jpg"
  },
  {
    name:"Gulati",
    id:"7",
    description: "The oldest and the most reputed restaurent in delhi. The food is famous throughout the world. Located at the prime location, its place you would wanna go as many times in life you can!",
    location:"link it",
    image:"assets/Gulati.jpg"
  },
  {
    name:"Karim’s jama masjid",
    id:"8",
    description: "The pre-Indian-Independence era restaurent. It is a treat for those who love a great chicken curry or lamb chops! They also make authentic curries from the 90's and it is hard to find a table on weekends!",
    location:"link it",
    image:"assets/Karim.jpg"
  },
  {
    name:"Blues",
    id:"9",
    description: "Are you looking for some soft-rock or country music vibes? Well! Blues is your place! They have live performances of yesteryear classics. All music lovers should try this place at Rajiv Chowk",
    location:"link it",
    image:"assets/Blues.jpg"
  },
  {
    name:"Olive Bar and Kitchen",
    id:"10",
    description: "This is definity the fanciest on the list! You will see numerous celebrity pics on their facebook page. It is mediterranean themed restaurent with renowed chefs and food!",
    location:"link it",
    image:"assets/oliver.jpg"
  },
  ];
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelhieatComponent } from './delhieat.component';

describe('DelhieatComponent', () => {
  let component: DelhieatComponent;
  let fixture: ComponentFixture<DelhieatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelhieatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelhieatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

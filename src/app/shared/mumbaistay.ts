import {Stay} from "../shared/stay"

export const stay:Stay[]=[{
  name:"Bandra",
  id:"1",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/the-trident-bandra-kurla.en-gb.html?aid=306395;label=bombay-gIurAvBFx9qfcMXxU%2AFKLAS154618436066%3Apl%3Ata%3Ap1480%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-125266192%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee", 
  img1:"../assets/bandra1.jpg",
  price1:"6500",

  hotel2:"https://www.booking.com/hotel/in/grace-galaxy.en-gb.html?aid=306395;label=bombay%2Fbandra-khar-4XhRTq3Hnx2D75bPvqfgcAS260810802696%3Apl%3Ata%3Ap120%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-739251126%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=509407303_215876571_0_1_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3445;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=2;highlighted_blocks=509407303_215876571_0_1_0;hpos=2;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=509407303_215876571_0_1_0__375900;srepoch=1597482398;srpvid=5804400f7a2f0001;type=total;ucfs=1&#hotelTmpl",
  price2:"1300",
  img2:"../assets/bandra2.jpg",
  hotel3:"https://www.booking.com/hotel/in/lucky-hotels-amp-restaurants.en-gb.html?aid=306395;label=bombay%2Fbandra-khar-4XhRTq3Hnx2D75bPvqfgcAS260810802696%3Apl%3Ata%3Ap120%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-739251126%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=113809202_139096995_2_0_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3445;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=7;highlighted_blocks=113809202_139096995_2_0_0;hpos=7;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=113809202_139096995_2_0_0__313600;srepoch=1597482398;srpvid=5804400f7a2f0001;type=total;ucfs=1&#hotelTmpl", 
  price3:"3500",
  img3:"../assets/bandra3.jpg",
  image:"assets/bandra.jpg"
},
{
  name:"Andheri",
  id:"2",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/deluxe-comfort.en-gb.html?aid=306395;label=bombay%2Fandheri-E1P%2AA_9fRP0FPHB52szWsgS383351239630%3Apl%3Ata%3Ap1145%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-739249296%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=363005506_203949172_2_1_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3712;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=363005506_203949172_2_1_0;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=363005506_203949172_2_1_0__185300;srepoch=1597482652;srpvid=0e52408dcbf601a7;type=total;ucfs=1&#hotelTmpl",  
  price1:"1800",
  img1:"../assets/andheri1.jpg",
  hotel2:"https://www.booking.com/hotel/in/radisson-mumbai-andheri-midc.en-gb.html?aid=306395;label=bombay%2Fandheri-E1P%2AA_9fRP0FPHB52szWsgS383351239630%3Apl%3Ata%3Ap1145%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-739249296%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=377748403_253817660_2_2_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3712;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=2;highlighted_blocks=377748403_253817660_2_2_0;hpos=2;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=377748403_253817660_2_2_0__550000;srepoch=1597482652;srpvid=0e52408dcbf601a7;type=total;ucfs=1&#hotelTmpl",
  price2:"5500",
  img2:"../assets/andheri2.jpg",
  hotel3:"https://www.booking.com/hotel/in/pacific-mumbai.en-gb.html?aid=306395;label=bombay%2Fandheri-E1P%2AA_9fRP0FPHB52szWsgS383351239630%3Apl%3Ata%3Ap1145%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-739249296%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=601037201_269774333_0_2_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3712;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=4;highlighted_blocks=601037201_269774333_0_2_0;hpos=4;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=601037201_269774333_0_2_0__215910;srepoch=1597482652;srpvid=0e52408dcbf601a7;type=total;ucfs=1&#hotelTmpl",
  price3:"2100",
  img3:"../assets/andheri3.jpg",
  image:"assets/andheri.jpg"
},
{
  name:"Mumbai Airport",
  id:"3",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/niranta-airport-transit-international-wing.en-gb.html?aid=324791;label=BOM-cvPvB3gwRKLimi7VIVDGKgS388461822221%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-11702656759%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YfOICo1LOVmeFX2Yjoqzz_A;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=178195501_265507795_0_0_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=-2092174;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=7;highlighted_blocks=178195501_265507795_0_0_0;hpos=7;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=178195501_265507795_0_0_0__584910;srepoch=1597483237;srpvid=4a0041b21f85020c;type=total;ucfs=1&#hotelTmpl",
  price1:"5300",
  img1:"../assets/airport1.jpg",
  hotel2:"https://www.booking.com/hotel/in/capital-o-29025-seven-olive.en-gb.html?aid=324791;label=BOM-cvPvB3gwRKLimi7VIVDGKgS388461822221%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-11702656759%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YfOICo1LOVmeFX2Yjoqzz_A;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=556545205_237811768_2_1_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=89;dest_type=airport;dist=0;group_adults=2;group_children=0;hapos=10;highlighted_blocks=556545205_237811768_2_1_0;hpos=10;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=556545205_237811768_2_1_0__209394;srepoch=1597483168;srpvid=c6dc418f2bc200c6;type=total;ucfs=1&#hotelTmpl",
  price2:"2500",
  img2:"../assets/airport2.jpg",
  hotel3:"https://www.booking.com/hotel/in/hilton-mumbai-international-airport.en-gb.html?aid=324791;label=BOM-cvPvB3gwRKLimi7VIVDGKgS388461822221%3Apl%3Ata%3Ap1%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-11702656759%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YfOICo1LOVmeFX2Yjoqzz_A;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=28362407_266007748_2_42_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=89;dest_type=airport;dist=0;group_adults=2;group_children=0;hapos=5;highlighted_blocks=28362407_266007748_2_42_0;hpos=5;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=28362407_266007748_2_42_0__595000;srepoch=1597483168;srpvid=c6dc418f2bc200c6;type=total;ucfs=1&#hotelTmpl", 
  price3:"5900",
  img3:"../assets/airport3.jpg",
  image:"assets/mumbaiairport.jpg"
},
{
  name:"Worli and south mumbai",
  id:"4",
  description:"If you are a tourist this is the most convienient place to stay. Long term staying might be expensive for middle class",
  hotel1:"https://www.booking.com/hotel/in/four-seasons-mumbai.en-gb.html?aid=306395;label=bombay%2Fworli-C%2AbynEYIp_RL3B71irVgkwS154620749706%3Apl%3Ata%3Ap13%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-12908490788%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=29344001_151472689_2_41_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3449;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=1;highlighted_blocks=29344001_151472689_2_41_0;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;sr_pri_blocks=29344001_151472689_2_41_0__950000;srepoch=1597484401;srpvid=57ba43f8fcc40021;type=total;ucfs=1&#hotelTmpl",
  price1:"9500",
  img1:"../assets/worli1.jpg",
  hotel2:"https://www.booking.com/hotel/in/shantiidoot-mumbai.en-gb.html?aid=306395;label=bombay%2Fworli-C%2AbynEYIp_RL3B71irVgkwS154620749706%3Apl%3Ata%3Ap13%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-12908490788%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=90570502_240263159_0_2_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3449;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=16;highlighted_blocks=90570502_240263159_0_2_0;hpos=16;no_rooms=1;room1=A%2CA;sb_price_type=total;spdest=di%2F3449;sr_order=popularity;sr_pri_blocks=90570502_240263159_0_2_0__359910;srepoch=1597484401;srpvid=57ba43f8fcc40021;type=total;ucfs=1&#hotelTmpl",  
  price2:"3500",
  img2:"../assets/worli2.jpg",
  hotel3:"https://www.booking.com/hotel/in/oyo-rooms-parel-near-tata-memorial-hospital.en-gb.html?aid=306395;label=bombay%2Fworli-C%2AbynEYIp_RL3B71irVgkwS154620749706%3Apl%3Ata%3Ap13%3Ap2%3Aac%3Aap%3Aneg%3Afi%3Atikwd-12908490788%3Alp1007751%3Ali%3Adec%3Adm%3Appccp%3DUmFuZG9tSVYkc2RlIyh9YZVcNNsENnH02-pWD53qm9c;sid=a9f46897da2eafd58958e31c6200c0ee;all_sr_blocks=182947702_270353685_2_0_0;checkin=2020-09-12;checkout=2020-09-13;dest_id=3449;dest_type=district;dist=0;group_adults=2;group_children=0;hapos=11;highlighted_blocks=182947702_270353685_2_0_0;hpos=11;no_rooms=1;room1=A%2CA;sb_price_type=total;spdest=di%2F3449;sr_order=popularity;sr_pri_blocks=182947702_270353685_2_0_0__238810;srepoch=1597484401;srpvid=57ba43f8fcc40021;type=total;ucfs=1&#hotelTmpl",
  price3:"1500",
  img3:"../assets/worli3.jpg",
  image:"assets/worli.jpg"
},


];
import {Rest} from '../shared/rest'


export const Restuarent:Rest[]=[{
  name:"The Table",
  id:"1",
  description: "Located just behind the Taj Hotel in Colaba, The Table is one of South Mumbai’s most stylish and unique restaurants. They have international chef. So,you can expect authentic cuisines!",
  location:"link it",
  image:"assets/thetable.jpg"
},
{
  name:"The Bombay Canteen",
  id:"2",
  description: "You want to go to a place that celebrates the Indian meal and small moments. Then this is for you! They have a bar offering innovative Indian snacks and colonial inspired cocktails. It cannot get more bombay than this",
  location:"link it",
  image:"assets/bombaycanteen.jpg"
},
{
  name:"Pali Bhavan",
  id:"3",
  description: "The have the calm lights that can calm your nerves to experience beauty. Serving the best momos in town! You cannot miss the chandeliers or the food!",
  location:"link it",
  image:"assets/palibhavan.jpg"
},
{
  name:"Sea Lounge at The Taj Mahal Hotel",
  id:"4",
  description: "Its sea facing lounge at Taj. The place carries Taj name and does justice to it. Almost all the delicacies are served on the menu with exquisite style. One of the oldest reputed places in mumbai. If you are a tourist please make a point to visit the place",
  location:"link it",
  image:"assets/taj.jpg"
},
{
  name:"Nutcracker",
  id:"5",
  description: "A quaint cafe for the artist and lover in you! They have art around the walls and organise events like NUTQWAKER NIGHTS. It is famous for its coffee,hot chocolate, pancakes and lebanese sandwich",
  location:"link it",
  image:"assets/nutcracker.jpg"
},
{
  name:"Britannia & Co.",
  id:"6",
  description: "Famous locale serving classic Parsi cuisine such as berry pulav in a vintage, colonial atmosphere.",
  location:"link it",
  image:"assets/britannia.jpg"
},
{
  name:"By the Way",
  id:"7",
  description: "The place offers exotic dishes like-mutton stew and steamed fish in mint chutney. Moreover, they contribute to Seva Sadan Society who help in education of disadvantaged girls. So, you would be eating as well as supporting a great cause!",
  location:"link it",
  image:"assets/bytheway.jpg"
},
{
  name:"Hakkasan",
  id:"8",
  description: "For chinese-food lovers here is your stop! Gives a Hong Kong vibes with its decor. Must stop for cravings for soft-shell carbs and dim-sums ",
  location:"link it",
  image:"assets/hakkasan.jpg"
},
{
  name:"Cafe Madras",
  id:"9",
  description: "Established 1940, the restaurent serves authentic South-Indian food. You can have a family meal without hurting your pockets!",
  location:"link it",
  image:"assets/cafemadras.jpg"
},
{
  name:"Carter Road Khau Galli",
  id:"10",
  description: "This is for people who love street food because you cannot cover all places in one day. Some of the places to start with in Khau galli  would be- Mohammed Ali Road,Carter Road,Zaveri Bazaar,Tardeo. It offers lip-smacking food from snacks to main-course, the abundance and spirit will make your day!",
  location:"link it",
  image:"assets/khau-gali.jpg"
},
];
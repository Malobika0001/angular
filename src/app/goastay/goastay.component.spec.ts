import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoastayComponent } from './goastay.component';

describe('GoastayComponent', () => {
  let component: GoastayComponent;
  let fixture: ComponentFixture<GoastayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoastayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoastayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
